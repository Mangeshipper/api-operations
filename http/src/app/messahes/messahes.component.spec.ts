import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessahesComponent } from './messahes.component';

describe('MessahesComponent', () => {
  let component: MessahesComponent;
  let fixture: ComponentFixture<MessahesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessahesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessahesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
